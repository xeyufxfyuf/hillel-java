import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class HomeWork4 {
    public static void main(String[] args) {

        Map<Integer, Integer> result = new HashMap<>();

        for(int i = 0; i < 100; i++) {
            Random ran = new Random();
            int randomNumber = ran.nextInt(10);
            if (result.containsKey(randomNumber)) {
                int value = result.get(randomNumber);
                result.put(randomNumber, value + 1);
            } else {
                result.put(randomNumber, 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : result.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
    }
}
